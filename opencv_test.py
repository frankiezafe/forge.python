import numpy as np
import cv2
import time

cap = cv2.VideoCapture('test.mp4')

image_count = 0

while(cap.isOpened()):
	
	ret, frame1 = cap.read()
	time.sleep(1/25)
	ret, frame2 = cap.read()
	img1 = cv2.absdiff(frame1,frame2)  # image difference
	
	gray = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
	blur = cv2.GaussianBlur(gray,(21,21),0)
	ret,thresh = cv2.threshold(blur,200,255,cv2.THRESH_OTSU)
	
	img2 = cv2.addWeighted(frame1,0.9,img1,0.1,0)
	
	contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)
	
	path = 'video_export/export_'
	if image_count < 10:
		path += "0000"
	elif image_count < 100:
		path += "000"
	elif image_count < 1000:
		path += "00"
	elif image_count < 10000:
		path += "0"
	path += str( image_count ) + ".png"
	
	cv2.imwrite( path, img2 )
	
	image_count += 1
	
	cv2.imshow('frame',img2)
	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

cap.release()
cv2.destroyAllWindows()