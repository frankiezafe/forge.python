import os
from PIL import Image, ImageDraw, ImageFont

font_family = ImageFont.truetype("FantasqueSansMono-Regular.otf", 20)
font_color = (255,255,255)

def watermark_text( input_image_path, output_image_path, pos ):
	
	photo = Image.open(input_image_path)
	text = os.path.basename( input_image_path )
	drawing = ImageDraw.Draw(photo)
	drawing.text(pos, text, fill=font_color, font=font_family)
	photo.save(output_image_path)
	print( output_image_path )

input_folder="/media/frankiezafe/Untitled/security"
output_folder="/media/frankiezafe/Untitled/security/watermarked"
extensions=[".jpg"]

allfiles = [f for f in os.listdir(input_folder) if os.path.isfile(os.path.join(input_folder, f))]
selectedfiles=[]
for f in allfiles:
	for e in extensions:
		if f[-len(e):] == e:
			absf = os.path.join(input_folder, f)
			selectedfiles.append( absf )
selectedfiles.sort()

i = 0
for f in selectedfiles:
	n = str(i)
	while len( n ) < 5:
		n = '0' + n
	watermark_text(f, os.path.join( output_folder, n + extensions[0] ), pos=(10, 450))
	i += 1

# img = '/home/frankiezafe/forge.python/watermark/2019-12-29_16.47.50_cam0_003.jpg'
# watermark_text(img, '2019-12-29_16.47.50_cam0_003_stamped.jpg', pos=(10, 450))
# ffmpeg -f image2 -framerate 20 -r 20 -i /media/frankiezafe/Untitled/security/watermarked/%6d.jpg -an -vcodec libx264 -qscale 1 -threads 7 /media/frankiezafe/Untitled/securit.mp4