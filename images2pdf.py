#!/usr/bin/env python3

'''
# INSTALLATION
pip install fpdf
sudo pip3 install fpdf
'''

import os, sys, time
from fpdf import FPDF

# A4
PAGE_WIDTH = 210
PAGE_HEIGHT = 297

IMAGE_EXT = ".png"
IMAGE_WIDTH = 200
IMAGE_HEIGHT = 280

if len(sys.argv) >= 2:
    SCAN_FOLDER = sys.argv[1]
    print( "SCAN_FOLDER:", SCAN_FOLDER )
    file_list = os.listdir(SCAN_FOLDER)
    image_list = []
    for file in file_list:
        if file[-len(IMAGE_EXT):] == IMAGE_EXT:
            image_list.append( { 'path': os.path.join(SCAN_FOLDER,file), 'name': file.replace(IMAGE_EXT,'') } )
    if len(image_list) > 0:
        # sorting image list
        image_list = sorted(image_list, key=lambda item: item['name'])
        pdf_path = os.path.join( SCAN_FOLDER, "export_"+str(len(image_list))+"imgs_"+str(int(time.time()))+".pdf" )
        posx = (PAGE_WIDTH-IMAGE_WIDTH)*0.5
        posy = (PAGE_HEIGHT-IMAGE_HEIGHT)*0.5
        pdf = FPDF( orientation = 'portrait', format = (PAGE_WIDTH,PAGE_HEIGHT) )
        pdf.set_margins(0,0,0)
        pdf.set_font('Arial', 'B', 10)
        i = 0
        max_char = len(str(len(image_list)))
        for image in image_list:
            pdf.add_page()
            pdf.image(image['path'],posx,posy,IMAGE_WIDTH,IMAGE_HEIGHT)
            pdf.rect(posx,posy,IMAGE_WIDTH,IMAGE_HEIGHT, 'D')
            title = str(i)
            while len(title) < max_char:
                title = '0'+title
            title += " : "+image['name']
            xoffset = pdf.get_string_width( title )
            pdf.set_xy(posx+1,posy+2.5)
            pdf.cell(0,0, title, 0, 0, 'L')
            i += 1
        pdf.output(pdf_path, "F")
        print( "pdf of ", len(image_list), " pages generated at ", pdf_path )   
