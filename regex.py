﻿import re

paths = [
	"models._model_normalized.obj",
	"model_normalized.obj"
]

skip = [ '^(.*?(\\._model_normalized\\b)[^$]*)$' ]

for p in paths:
	for s in skip:
		match = re.findall(s,p)
		if len( match ) > 0:
			print( p + " MATCH!" )